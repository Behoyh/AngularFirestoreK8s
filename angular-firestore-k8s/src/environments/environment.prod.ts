export const environment = {
  production: true,
  firebase: {
    apiKey: '<APIKEY>',
    authDomain: '<AUTHDOMAIN>',
    databaseURL: '<BASEURL>',
    projectId: '<PROJECTID>',
    storageBucket: '<STORAGEBUCKET>',
    messagingSenderId: '<MESSAGESENDERID>'
  }
};
